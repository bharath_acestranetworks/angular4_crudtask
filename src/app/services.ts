import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@Injectable()
export class Service {

    id:number;

    public static API_URL = 'http://localhost:5555/';

    public headers: Headers;

    constructor(private http: Http, ) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json')
    }

    post_list(posturl,obj): Observable<any> {


        try { 
            console.log(Service.API_URL + posturl,obj)
            return this.http.post(Service.API_URL + posturl,obj)   
                .map(this.extractPostData)
                .catch(this.handlePostError);
        } catch (error) { console.log(error); }
    }
  
    extractPostData(res: any) {
        const body = res.json();
        console.log(body);
        return body || {};
    }
    handlePostError(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server   error';
        return Observable.throw(error);
    }

    get_list(getlisturl): Observable<any> {

        try {
            console.log(Service.API_URL + getlisturl);
            return this.http.get(Service.API_URL + getlisturl)   
                .map(this.extract_get_list)
                .catch(this.handle_get_list_error);
        } catch (error) { console.log(error); }
    }
    extract_get_list(res: any) {
        console.log(res);
        const body = res.json();
        console.log(body);
        return body || {};
    }
    handle_get_list_error(error: any) {
        console.log(error);
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server   error';
        return Observable.throw(error);
    }


    get_data(getdata_url,user_id): Observable<any> {
        try {
            return this.http.get(Service.API_URL + getdata_url)   
                .map(this.extract_get_data_result)
                .catch(this.handle_get_data_error);
        } catch (error) { console.log(error); }
    }
    extract_get_data_result(res: any) {
        const body = res.json();
        console.log(body);
        return body || {};
    }
    handle_get_data_error(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server   error';
        return Observable.throw(error);
    }
    
    put_data(update_url,id,obj): Observable<any> {
        try {

            const api_url = Service.API_URL + update_url;
            const url = `${api_url}/${id}`;
            return this.http.put(url,JSON.stringify(obj),{headers: this.headers})
                .map(this.extract_put_data_result)
                .catch(this.handle_put_data_error);
        } catch (error) { console.log(error); }
    }
    extract_put_data_result(res: any) {
        const body = res.json();
        
        return body || {};
    }
    handle_put_data_error(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server   error';
        return Observable.throw(error);
    }
   
    delete_data(delete_url,id): Observable<any> {
        try {

            const api_url = Service.API_URL + delete_url;
            const url = `${api_url}/${id}`;
            return this.http.delete(url,{headers: this.headers})
                .map(this.extract_delete_data_result)
                .catch(this.handle_delete_data_error);
        } catch (error) { console.log(error); }
    }
    extract_delete_data_result(res: any) {
        const body = res.json();
        console.log(body);
        return body || {};
    }
    handle_delete_data_error(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server   error';
        return Observable.throw(error);
    }
   
    
    ngOnInit() {


    }

}

