import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Service } from '../services';

@Component({
  selector: 'app-crud-operation',
  templateUrl: './crud-operation.component.html',
  styleUrls: ['./crud-operation.component.css'],
  providers: [Service]
})
export class CrudOperationComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private api_service: Service) { }
  form: FormGroup;
  contactgroup: FormGroup;
  update_form: boolean;
  link_url: string = "user_list";
  user_table: any;
  users: any;
  user: object = {};
  ngOnInit() {
    this.FormInit();
    this.getlist(this.link_url);
  }
  drop_roles = [{
    "role_name": "Student",
  },
  {
    "role_name": "Employee",
  }
  ]


  FormInit() {
    this.form = this.formBuilder.group({

      user_name: new FormControl('', [Validators.required,Validators.pattern('[a-zA-Z ]*'), Validators.minLength(3),]),
      role_name: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      contact_sms: new FormControl(''),
      contact_email: new FormControl(''),
    });
    this.update_form = false;
    

  }
  getlist(url) {
    this.api_service.get_list(url)
      .subscribe((result: any) => {
        console.log(result);
        this.user_table = result;
      },
        (error) => {
          console.log(error);
        });
  }
  onSubmit() {
    console.log(this.form.value);

    if (this.form.valid) {
      console.log(this.form.value)
      this.api_service.post_list(this.link_url, this.form.value)
        .subscribe((result: any) => {
          this.form.reset();
          this.getlist(this.link_url);
        },
          (error) => {
            console.log(error);
          });
    } else {
     
      this.validateAllFormFields(this.form);
    }

  }

  edit_user(id) {
    console.log(id);
    this.api_service.get_data(this.link_url, id)
      .subscribe((result: any) => {
        this.users = result;
        console.log(this.users);
        for (var i = 0; i < this.users.length; i++) {

          if (parseInt(this.users[i].id) === id) {

            this.user = this.users[i];
            this.update_form = true;

            break;
          } else {
            this.update_form = false;
          }
        }
        console.log(this.user);
      },
        (error) => {
          console.log(error);
        });
  }
  update_user(id) {
    console.log(this.form.value);

    if (this.form.valid) {
      this.api_service.put_data(this.link_url, id, this.form.value)
        .subscribe((result: any) => {
          this.update_form = false;
          this.form.reset();
          this.getlist(this.link_url);
        },
          (error) => {
            console.log(error);
          });
    } else {
     
      this.validateAllFormFields(this.form);
    }
  }
  delete_user(id) {
    this.api_service.delete_data(this.link_url, id)
      .subscribe((result: any) => {
        this.getlist(this.link_url);
      },
        (error) => {
          console.log(error);
        });
  }
  reset() {
    this.form.reset();
    this.update_form = false;
  }
  validation_messages = {
    'user_name': [
      { type: 'required', message: 'User Name is required.' },
      { type: 'pattern', message: 'Numbers not allowed.' },
      { type: 'minlength', message: 'Minimum 3 letters required' }
    ],
    'role_name': [
      { type: 'required', message: 'Role Name is required.' }
    ],
    'gender': [
      { type: 'required', message: 'Gender Type is required.' }
    ],
    'description': [
      { type: 'required', message: 'Description is required.' }
    ],
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  

}
